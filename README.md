# subnet_radar

Scans a subnet for existing IP hosts and executes user-scripts when its **disconnected**, **alone** or **not_alone**.

Its possible to ignore specific hosts, by adding their MAC addresses to file IGNORED_HOSTS.txt

The scans are made using `nmap -sP`, so nmap needs to be installed

The original motivation to create this program was to automatically enable/disable torrent or emule or other bandwidth-intensive-applications which should run only when no one else is using your internet connection (your private subnet) - this is possible and easy with subnet_radar. It can also be used as a (very simple) detection mechanism against (very simple) intruders in a wireless network.

If it occurs you it might be usefull for anything else, please let me know through https://github.com/zipizap

### Install
    sudo apt-get install ruby1.9.1
    sudo apt-get install nmap
    git clone git@github.com:zipizap/subnet_radar.git



### Usage

1. Place your executable files inside the **state subdirs**:
  + .../subnet\_radar/on\_alone
  + .../subnet\_radar/on\_not\_alone
  + .../subnet\_radar/on\_disconnect

2. If you want to ignore some host, add their MACs into file **IGNORED_HOSTS.txt**

3. Run subnet_radar.rb, like `ruby subnet_radar.rb wlan0`


### Detailed description

<table>
  <tr>
    <th>State</th>
    <th># accounted hosts</th>
    <th>State user-script subdir</th>
  </tr>
  <tr>
    <td>disconnect</td>
    <td>less than 2</td>
    <td>.../subnet_radar/on_disconnect/</td>
  </tr>
  <tr>
    <td>alone</td>
    <td>== 2 (me and router)</td>
    <td>.../subnet_radar/on_alone/</td>
  </tr>
  <tr>
    <td>not_alone</td>
    <td>> 2</td>
    <td>.../subnet_radar/on_not_alone/</td>
  </tr>
</table>

This program will periodically make a scan to discover _all existing hosts_ in the interface's subnet.

The _all existing hosts list_ will be filtered out to ignore hosts present in the file IGNORED\_HOSTS.txt (by MAC), resulting in a _accounted hosts list_ which contains all existing hosts not ignored

Finally the _accounted hosts list_ will be interpreted into one of 3 states: **disconnect**, **alone** or **not\_alone**. The interpretation is based on the number of _accounted hosts_ (see table above)


The program will periodically launch scans, each scan resulting in its own state. When from an old scan to a new scan there is a state change, from an old\_state ==>> new\_state, the program will launch the new\_state's _user-scripts_.

The _user-scripts_ for each state are all the **executable** files found in the **state subdirs**:

  + .../subnet\_radar/on\_alone
  + .../subnet\_radar/on\_not_alone
  + .../subnet\_radar/on\_disconnect

The _user-scripts_ placed in the _state subdirs_ should be created by the user: it's how you costumize the actions, what will happen when its **alone**, or **not_alone** or **disconnect** 


Above all, this program is very simple: it detects _state_ changes and launches _user-scripts_ on new _states_



### Usage examples

#### Autothrottle a domestic torrent server so when it is alone it will use all the bandwidth, and when it is not_alone it uses only a litle of bandwidth

At home, I have a server running transmission (torrent client) to download torrents. 
I want to make it, so that:

  A). when the server is **alone** in the network, ie no other computer is using internet, transmission should use all available bandwidth
  
  B). when the server is **not_alone** in the network, ie there are other home computers connected, tranmission should use only a small amount of bandwidth, to leave the remaining bandwidth available for the other computers (so they don't lagggg)
  
  C). my android mobile is always connected to the wireless at home, and I want it to be _ignored_


Transmission can be controled by console, so its easy to change the transmission bandwidth usage, by using 2 scripts: "transmission\_speedup.sh" and "transmission\_slowdown.sh"

----

----


The setup of subnet_radar for this is simple: 

  A) + B):  just place the transmission scripts in the correct **states_ subdirs** 

  C)      add the android-phone MAC into the file **IGNORED_HOSTS.txt**


    .../subnet_radar/ 
        |
        |--- on_alone/
        |         |--- transmission_speedup.sh
        |
        |--- on_not_alone/
        |         |--- transmission_slowdown.sh
        |
        |--- on_disconnect/
        |
        |--- IGNORED_HOSTS.txt  (with android-phone MAC)
    
With this setup, when the server detects that is **alone** it will execute the script:  
    ` .../subnet_radar/on_alone/transmission_speedup.sh`    
, and when it switches to **not\_alone** it will execute:    
    `.../subner_radar/on_not_alone/transmission_slowdown.sh` 

The android-phone will always be ignored, so if for instance, in a moment we have the router + server + android connected, subnet_radar will ignore the android because its MAC is inside the file **IGNORED\_HOSTS.txt**, and will only account the router + server, and so decide that it is **alone**



### Final notes
  - made and testes with ruby 1.9.3p194
  - depends of nmap: uses nmap to make ping scans, it does not uses any ruby library/gem,
  - very few resources: low cpu, low mem, each scan uses a 8KB spike of upload-bandwidth
  - ?reliability?: don't know to which point this could be reliable, only tested it in these simple situations




### License
Licensed under GPLv3

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

