#! /home/paulo/.rvm/wrappers/default/ruby
#
#NOTE about shebang line:
#   -> If you have ruby through RVM, then your shebang line should be:
#         #! /home/YOURUSERNAME/.rvm/wrappers/default/ruby
#      Like this, you can call this script:
#         - manually when you are logged in, in a terminal
#         - automatically from other scripts being run with your user, that call this script
#         - automatically from boot-process scripts (such as /etc/rc.local or /etc/init.d/* ) 
#
#
#   -> If you have ruby installed system-wide, then your shebang line should instead be:
#         #! /usr/bin/env ruby 

#NOTE made with ruby 1.9.3



#DONE:
# - add colors to printout
# - improve debug messages, they are too messy
# - option to ignore hosts by MAC (ex: ignore mobiles)
# - improve output text on state transitions, to show number of ignored hosts
# - scan_subnet now makes 3 consecutive scans and considers the compound result, for better reliability (avoiding wireless false-negatives)
#
#
#
#TODOs:
# - replace nmap by linux ping, or Ruby ping
# - properly log and save into file.log
# - add option --log_show
# - add option --log_save_into_file
# - autoguess interface, through reading interface which connects to default gw (interface argument = guess )
#



module SubnetRadar
  VERSION = File.read(File.join(File.expand_path(File.dirname(__FILE__)),'VERSION')).lines.to_a[0].chomp     # "0.1"
  @debug_active = false
  @polling_interval = 5    #Integer, minutes
  extend self 

  def run
    banner = <<EOT
  SubnetRadar v#{VERSION}   

  Usage: #{$0} [-d|--debug] [-pi=N|--polling_interval=N] <INTERFACE>  

  Where:
     N           : polling interval in minutes (0,1,2,3,... defaults is 5)
     <INTERFACE> : ifconfig interface (ex: wlan0, eth0, ...)

  Example:
      #{$0} wlan0

EOT

    # check args
    if ARGV.empty?
      Shw.info banner
      Shw.err "too few arguments!"
      exit 1
    elsif not ( host_interface = ARGV[-1].downcase ) =~ /^(wlan|eth)\d$/i
      Shw.info banner
      Shw.err "interface '#{ARGV[-1].downcase}' is not expected (wlanX or ethX)"
      exit 1
    end
    
    if ARGV.select {|o| o  =~ /^(--debug|-d)$/i }[-1]
      Shw.dbg "Command line argument --debug detected: activating debug"
      @debug_active = true 
    end

    if ( opt_txt = ARGV.select {|o| o =~ /^(-pi=|--polling_interval=)\d$/i }[-1] )
      @polling_interval = opt_txt[/\d/].to_i 
      Shw.dbg "Command line argument --polling_interval detected with #{@polling_interval} mins"
    end

    # check that host_interface exists in ifconfig
    # TODO
   
    Shw.dbg "Preparing trap for CTRL-C"
    trap("SIGINT") do 
      Shw.dbg "Caught CTRL-C, exiting now"
      Shw.warn "Exit\n"
      exit 0 
    end


    #              Detected states:
    #                  :disconnect      
    #                  :alone              
    #                  :not_alone          
    #              
    #              Transition diagram:
    #             
    #                     :disconnect
    #                       /        \
    #                      /          \
    #                  :alone   --  :not_alone
    #             
    #              When a transition into a new state happens, it will be called #on_<new_state>, ex: #on_not_alone(num_hosts_accounted, interface_data)
    #
    #Initial state 
    state = :disconnect
    Shw.dbg "Initial state is: #{state}"

    # main loop (run infinitely until CTRL-C is trapped)
    Shw.dbg "Starting main loop"
    Shw.norm "#{Time.now}\t Starting to scan every \e[1;33m#{@polling_interval}\e[0m min, the subnet of interface \e[1;34m#{host_interface}\e[0m"
    Shw.norm "(To stop press \e[1;37mCTRL-C\e[0m or kill this process (\e[1;37m#{Process.pid}\e[0m) with something like 'sudo kill -9 #{Process.pid}' :) )\n\n"
    while true
      Shw.dbg ""
      Shw.prnt "."

      # Launch radar sweep
      Shw.dbg "\t\tLaunching radar sweep"
      radar_results = radar_sweep(host_interface)
      Shw.dbg "Got radar_results = '#{radar_results.inspect}'"

      #check the num_hosts_accounted to see if we are alone or not
      Shw.dbg "Going to interpret current state from radar_results, and detect state transitions"
      
      state_proc = Proc.new do |state_sym, state_dbg_msg|
        old_state = state
        state     = state_sym
        Shw.dbg "Current state = '\e[1;36m#{state}\e[0m'"
        Shw.dbg "  --> " + state_dbg_msg
        if state != old_state
          Shw.dbg "State Transition detected, from '#{old_state}' --> '#{state}'"
          state_method_handler_sym = ("on_" + state_sym.to_s).to_sym
          send(state_method_handler_sym, radar_results) 
        end
      end

      case 
        when radar_results.nil?
          state_proc.call( :disconnect, "Scan did not produced accurate results - maybe we are disconnected?" )

        when radar_results[:accounted_hosts].size == 2
          state_proc.call( :alone,      "We are ALONE with the router in the subnet! (filtered #{radar_results[:ignored_hosts].size} ignored MACs)" )

        when radar_results[:accounted_hosts].size > 2
          state_proc.call( :not_alone,  "We are NOT ALONE with the router in the subnet! (filtered #{radar_results[:ignored_hosts].size} ignored MACs)" )
      end #case

      # wait @polling_interval minutes
      Shw.dbg "Waiting '#{@polling_interval}' minutes..."
      sleep 60*@polling_interval
    end #while 1

  end # run 

  private  
  # This is a helper function to be used as needed by the handlers on_alone, on_not_alone, on_disconnect
  # It will:
  #   - display in stdout a message indicating the new state name
  #   - run all executable files present in ./on_<new_state_name> (ex: ./on_alone)
  #
  # Arguments
  #     new_state_sym         Symbol with the name of the new state (ex: :alone, :not_alone, :disconnect)
  #     radar_results         Hash      (same type as returned by radar_sweep)
  # 
  # Returns
  #     nil
  #
  def on_generalhandler(new_state_sym, radar_results)
    new_state_name = new_state_sym.to_s # "alone"

    #   - display in stdout a message indicating the new state name
    Shw.warn "\n"+"-"*60
    Shw.warn "#{Time.now}\t #{new_state_name.upcase}\n"
    fit_txt = Proc.new do |txt, fit_size|
      txt = txt[0,fit_size] if txt.size > fit_size        #trim txt to be at most as big as fit_size
      txt_size   = txt.size
      space_size = fit_size - txt_size
      left_space  = ( space_size / 2 ) + ( space_size % 2 )
      right_space = space_size - left_space
      (" "*left_space) + txt + (" "*right_space) 
    end
    if radar_results
      Shw.warn [fit_txt["IP", 15],fit_txt["MAC", 17],fit_txt["Ignored?", 7]]*"\t"
      Shw.warn radar_results[:all_hosts].map {|e| e.values*"\t"}*"\n"
      Shw.warn "\t\e[1;34m#{radar_results[:all_hosts].size}\e[0m total hosts"
      Shw.warn "\t\e[1;30m#{radar_results[:ignored_hosts].size}\e[0m ignored hosts"
      Shw.warn "\t\e[1;32m#{radar_results[:accounted_hosts].size}\e[0m accounted hosts"
    end
    Shw.warn "-"*60
    Shw.dbg "===> Switched to #{new_state_name.upcase}" 

    #   - run all executable files present in ./on_<new_state_name>
    scripts_dir = File.join(File.expand_path(File.dirname($0)),"on_#{new_state_name}")
    Dir.foreach(scripts_dir) do |entry|
      entry = File.join(scripts_dir,entry)
      execute_file(entry)
    end
  end
  
  #called when there is a state-transition into :alone
  def on_alone(radar_results)
    new_state_sym = :alone
    on_generalhandler(new_state_sym, radar_results)
  end

  #called when there is a state-transition into :not_alone
  def on_not_alone(radar_results)
    new_state_sym = :not_alone
    on_generalhandler(new_state_sym, radar_results)
  end

  #called when there is a state-transition into :disconnect
  def on_disconnect(radar_results)
    new_state_sym = :disconnect
    on_generalhandler(new_state_sym, radar_results)
  end

  
  # This function will:
  #     - read interface data, such as ip, subnetprefixlength, etc
  #       .if read fails, return nil
  #
  #     - scan entire subnet and define 
  #         all_hosts =       [ { ip:       "192.168.1.1"
  #                             }
  #                           ]
  #       .if scan fails, return nil
  #
  #     - map IPs -> MACs and update
  #         all_hosts =       [ { ip:       "192.168.1.1",
  #                               mac:      "00:3F:2G:11:22:33"
  #                             }
  #                           ]
  #
  #     - read file IGNORED_HOSTS.txt and update
  #         all_hosts =       [ { ip:       "192.168.1.1",
  #                               mac:      "00:3F:2G:11:22:33",
  #                               ignore:   true/false
  #                             }
  #                           ]
  #
  #     - define and return
  #         radar_results = { all_hosts:         all_hosts = [ { ip:       "192.168.1.1",        #
  #                                                              mac:      "00:3F:2G:11:22:33",
  #                                                              ignore:   true/false
  #                                                            },
  #                                                            ...
  #                                                          ],
  #
  #                          ignored_hosts:                  [ { ip:       "192.168.1.1",        #  #                                                        
  #                                                              mac:      "00:3F:2G:11:22:33",
  #                                                              ignore:   true
  #                                                            },
  #                                                            ...
  #                                                          ],
  #
  #                          accounted_hosts:                [ { ip:       "192.168.1.1",        #  #                                                        
  #                                                              mac:      "00:3F:2G:11:22:33",
  #                                                              ignore:   false
  #                                                            },
  #                                                            ...
  #                                                          ],
  #                        }
  #
  #
  #
  #
  # Arguments:
  #     interface String    ex: "wlan0" or "eth1"
  #
  # Return 
  #     nil 
  #     or 
  #     Hash = radar_results  (see above)
  #            
  #
  #
  def radar_sweep(interface)
    # This function will:
    #     - read interface data, such as ip, subnetprefixlength, etc
    #       .if read fails, return nil
    Shw.dbg "Reading the '#{interface}' interface data to get this host info, like ip, subnetprefixlength, etc"
    interface_data = read_interface(interface)
    Shw.dbg " interface_data = #{interface_data.inspect}"
    return nil unless interface_data

    #
    #     - scan entire subnet and define 
    #         all_hosts =       [ { ip:       "192.168.1.1"
    #                             }
    #                           ]
    #       .if scan fails, return nil
    subnet = interface_data.values_at(:ip,:mask_pref_length)*"/"
    Shw.dbg "Scanning the subnet '#{subnet}' to discover all hosts ip addresses"
    hosts_ip_arr = scan_subnet(subnet) 
    return nil unless hosts_ip_arr
    all_hosts = hosts_ip_arr.map {|h_ip| {ip: h_ip}}
    Shw.dbg "all_hosts = #{all_hosts.inspect}"

    #
    #     - map IPs -> MACs and update
    #         all_hosts =       [ { ip:       "192.168.1.1",
    #                               mac:      "00:3F:2G:11:22:33"
    #                             }
    #                           ]
    all_hosts.each do |h| 
      if h[:ip] == interface_data[:ip] 
        #fix to get mac from interface_data, because resolve_ip_to_mac cannot resolve own ips from same self-host 
        h[:mac] = interface_data[:mac] 
      else
        h[:mac] = resolve_ip_to_mac(h[:ip])
      end
    end
    Shw.dbg "Resolved the IPs -> MACs"
    Shw.dbg "all_hosts = #{all_hosts.inspect}"
    return nil if not all_hosts.select {|h| h[:mac].nil?}.empty?

    #
    #     - read file IGNORED_HOSTS.txt and update
    #         all_hosts =       [ { ip:       "192.168.1.1",
    #                               mac:      "00:3F:2G:11:22:33",
    #                               ignore:   true/false
    #                             }
    #                           ]
    Shw.dbg "Reading IGNORED_HOSTS.txt and filtering ignored MACS"
    txt = File.read(File.join(File.expand_path(File.dirname(__FILE__)),'IGNORED_HOSTS.txt'))
    macs_ignored_arr = txt.lines.to_a.
                        select {|l| (not l =~ /^\s*#/) and (not l =~ /^\s*$/) }.
                        map {|l| l.upcase.chomp.gsub(/\s/,'')}.
                        select {|m| m =~ /^(\h{2}:){5}\h{2}$/ }
    Shw.dbg "macs_ignored_arr = '#{macs_ignored_arr.inspect}'"
    all_hosts.each {|h| h[:ignore] = !!macs_ignored_arr.find_index(h[:mac]) }
    Shw.dbg "all_hosts = #{all_hosts.inspect}"

    #
    #     - define and return
    #         radar_results = { all_hosts:         all_hosts = [ { ip:       "192.168.1.1",        #
    #                                                             mac:      "00:3F:2G:11:22:33",
    #                                                             ignore:   true/false
    #                                                           },
    #                                                           ...
    #                                                         ],
    #
    #                          ignored_hosts:                 [ { ip:       "192.168.1.1",        #  #                                                        
    #                                                             mac:      "00:3F:2G:11:22:33",
    #                                                             ignore:   true
    #                                                           },
    #                                                           ...
    #                                                         ],
    #
    #                          accounted_hosts:               [ { ip:       "192.168.1.1",        #  #                                                        
    #                                                             mac:      "00:3F:2G:11:22:33",
    #                                                             ignore:   false
    #                                                           },
    #                                                           ...
    #                                                         ],
    #                        }
    radar_results = {}
    radar_results[:all_hosts]       = all_hosts
    radar_results[:ignored_hosts]   = all_hosts.select {|h| h[:ignore] } 
    radar_results[:accounted_hosts] = all_hosts.select {|h| not h[:ignore] } 
    return radar_results
  end

  # Return 
  #   Hash or nil
  #         Hash = { interface:             "wlan0"                #String interface name
  #                  ip:                    "1.2.3.4",             #String Ip Address
  #                  mac:                   "74:2F:68:85:DD:94"    #String MAC Address
  #                  bcast:                 "1.2.3.255",           #String Broadcast Address
  #                  mask:                  "255.255.255.0"        #String Subnet Mask
  #                  mask_pref_length:      24                     #Integer Subnet-mask prefix length
  #                } 
  #
  def read_interface(interface)
    hsh={}
    ifc_stdout = %x(ifconfig #{interface} 2>/dev/null ).chomp
    mac = (ifc_stdout.lines.to_a[0]||"").split[-1][/^(\h{2}:){5}\h{2}$/].upcase
    #Shw.dbg "mac=#{mac.inspect}"
    ip_data = (ifc_stdout.lines.to_a[1]||"").split 
    return nil if ip_data.empty?
      # ip_data = 
      # [
      #     [0] "inet",
      #     [1] "addr:192.168.1.11",
      #     [2] "Bcast:192.168.1.255",
      #     [3] "Mask:255.255.255.0"
      # ]
    hsh.merge!( { name:               interface,
                  mac:                mac,
                  ip:                 ip_data[1].split(':')[1],
                  bcast:              ip_data[2].split(':')[1],
                  mask:               ip_data[3].split(':')[1]
               } )
    hsh[:mask_pref_length] = (hsh[:mask]||"").split('.').map {|s| s.to_i.to_s(2).gsub('0','')}.join.size
    #Shw.dbg "hsh = #{hsh.inspect}"
    return ( hsh.values.compact.empty? ?  nil : hsh )
  end
  
  # Description:
  #   This function scans the entire subnet to discover hosts alive
  #   The scan is made with 'nmap -sP' which is a "fortified" ping scan
  #   To increase the reliability, 3 consecutive scans are made and the compound result is considered. (to reduce false negatives that oftern appear on wireless subnets)
  #
  # Arguments:
  #     subnet    String    ex: "192.168.1.11/24"  
  #                         Must have format  "<IpAddress>/<SubNetPrefixLength>"
  # Return 
  #       Array of IPs = ["192.168.1.1", 
  #                       "192.168.1.2",
  #                       ...
  #                      ]
  #       or 
  #       nil if the scan failed
  #
  # Needs 'nmap' installed and available in path
  #
  def scan_subnet(subnet)
    subnet.gsub!("\\",'/')
    return nil unless subnet =~ /^\d+\.\d+\.\d+\.\d+\/\d+$/
    ip_regexp    = /(\d+\.){3}\d+/
    hosts_ip_arr = []
    num_of_scans = 3
    (1..num_of_scans).each do |i|
      this_nmap_stdout = %x(nmap -sP #{subnet}) 
      #Shw.dbg "nmap_stdout = '#{nmap_stdout}'"
      this_hosts_ip_arr = this_nmap_stdout.lines.to_a.map {|l| l[ip_regexp]}.compact  
        # ["1.2.3.4", ...] or []
      Shw.dbg "scan ##{i}/#{num_of_scans}\t\tthis_hosts_ip_arr = '#{this_hosts_ip_arr.inspect}'"
      hosts_ip_arr |= this_hosts_ip_arr
      #Shw.dbg "host_ip_arr = '#{hosts_ip_arr.inspect}'"
    end
    #hosts_ip_arr now has the composed results from 3 scans
    return nil if ( hosts_ip_arr.nil? || hosts_ip_arr.empty? )
    return hosts_ip_arr
  end

  # This function will resolve an IP into a MAC, by using the linux command "arp -an <IP>"
  # NOTE: it cannot resolve MACs of itself, only external MACs from other hosts
  #
  # Arguments:
  #   String        Ip as a string, ex: "192.168.1.19"
  #
  # Returns:
  #   String        MAC of the IP, as a string in upcase, ex: "00:10:FF:G2:A2:C7"
  #   or
  #   nil           if the MAC could not be found for the given IP
  #
  # Uses linux "arp" program, from net-tools debian package
  # 
  def resolve_ip_to_mac(ip)
    return nil unless ip =~ /^\d+\.\d+\.\d+\.\d+$/
    arp_stdout = %x(arp -na #{ip})
    mac_regexp = /(\h{2}:){5}\h{2}/
    mac = arp_stdout[mac_regexp]     #mac will be the MAC-string or nil if MAC was not found
    mac.upcase! unless mac.nil?
    return mac
  end

  def execute_file(file)
    if File.file?(file) && File.executable?(file)
      Shw.warn "Executing: #{file}"
      Shw.norm %x(#{file}     2>&1).gsub(/^/,'>> ') 
      Shw.norm ""
    end
  end

  #
  #This will attr_reader the module-obj-instance-var @debug_active, so that outside the module, the @debug_active is readable
  class << self
    attr_reader :debug_active
  end
end



module Shw
  extend self
  def norm(msg)
    color_code = "\e[0m"
    shw(msg,color_code)
  end
  def info(msg)
    color_code = "\e[1;34m"
    shw(msg,color_code)
  end
  def warn(msg)
    color_code = "\e[1;33m"
    shw(msg,color_code)
  end
  def err(msg)
    color_code = "\e[1;31m"
    shw(("ERROR: " + msg ), color_code)
  end
  def prnt(msg)
    color_code = "\e[37m"
    shw(msg, color_code,use_print:true)
  end
  def dbg(msg)
    color_code = "\e[1;30m"
    msg = "Debug #{Time.now}\t" + msg
    shw(msg,color_code) if SubnetRadar.debug_active
  end

  private
  def shw(msg,color_code,opts={})
    #init values
    output_method = :puts

    #opts overriding
    output_method = :print if opts[:use_print]

    #execute
    send(output_method, (color_code + msg + "\e[0m"))
  end
end



SubnetRadar.run

