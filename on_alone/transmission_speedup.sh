#!/bin/bash
function shw_grey { echo -e '\033[1;30m'"$1"'\033[0m'; }
function shw_norm { echo    "$1"; }
function shw_info { echo -e '\033[1;34m'"$1"'\033[0m'; }
function shw_warn { echo -e '\033[1;33m'"$1"'\033[0m'; }
function shw_err  { echo -e '\033[1;31mERROR: '"$1"'\033[0m'; }

# DeActivate alt speed
shw_info " +++ Raising transmission bandwidth"

# With authentication
#
# USERNAME="terraza"
# PASSWORD="terraza"
#transmission-remote --auth $USERNAME:$PASSWORD --no-alt-speed

# Without authentication (the default)
#
transmission-remote --no-alt-speed
